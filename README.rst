==============================
confluent-cloud-creds-rotation
==============================

Lambda Function to rotate Confluent Cloud kafka SASL credentials in Secrets Manager

This only works if you create your clients secrets in AWS Secrets manager, and we highly recommend to use
``confluent-cloud-clients-secrets.yaml`` template to do so.

The function is expecting to be able to find specific keys in the secret string (in JSON Format) and will otherwise fail.

How to use / install
=======================

1. First off, create the Confluent Cloud master API keys secret, using the template ``ccloud_master_secret.yaml``

.. code-block:: bash

    # Create the API key using the confluent CLI
    confluent api-key create --resource cloud

    # Create the stack in your account to create the Confluent API secret
    # make sure to replace API_KEY and API_KEY with the values obtained in above command
    aws cloudformation deploy --template-file ccloud_master_secret.yaml --stack-name ccloud-api-key \
    --parameter-overrides ConfluentCloudApiKey=<API_KEY> ConfluentCloudSecretKey=<API_KEY>

Note the secret ARN for the next steps, noted as ``SECRET_ARN``

2. Build and deploy the lambda function for rotation

.. hint::

    If you do not yet have a S3 bucket to store the lambda function code, we recommend to create a new bucket
    using the template ``s3.yaml``

For the value of ``ConfluentSecretsArns``, if you use our template ``ccloud_master_secret.yaml``, the value will be
in the form of

..

    arn:${AWS::Partition}:secretsmanager:${AWS::Region}:${AWS::AccountId}:secret:/kafka/${AWS::Region}/lkc-*

.. code-block::

    # Build the layer, with all the dependencies and the lambda function code
    AWS_PROFILE=${AWS_PROFILE:-default} BUCKET=<bucket_name> make upload-zip-s3

3. Deployment of the function and access to Confluent API keys secret

A. If your rotation function is the same account as SECRET_ARN

.. code-block::

    # Deploy to your account

    aws --profile ${AWS_PROFILE:-default} cloudformation deploy --capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
    --template-file ./template.json --stack-name lamba-fn--ccloud-secrets-rotation \
    --parameters-override ConfluentApiMasterSecretArn=<SECRET_ARN> \
    ConfluentSecretsArns="arn:aws:secretsmanager:eu-west-1:0123456789012:secret:/kafka/eu-west-1/lkc-*"


B. If the rotation function is in account A and ``SECRET_ARN`` is in account B

.. code-block::

    # Deploy function to your account A

    aws --profile ${AWS_PROFILE:-default} cloudformation deploy --capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
    --template-file ./template.json --stack-name lamba-fn--ccloud-secrets-rotation \
    --parameters-override ConfluentApiMasterSecretArn=<SECRET_ARN> \
    ConfluentSecretsArns="arn:aws:secretsmanager:eu-west-1:0123456789012:secret:/kafka/eu-west-1/lkc-*"

Take note of the Lambda function Role ARN, which will be noted ``FUNCTION_ROLE_ARN`` in the next step

.. code-block::

    # Create IAM Cross Account Role in account B, same as where `SECRET_ARN` is.

    aws --profile ${AWS_PROFILE:-default} cloudformation deploy --capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
    --template cross_account_iam_role.yaml --parameters-override \
    ConfluentApiMasterSecretArn=<SECRET_ARN> \
    RotationFunctionRoleArn=<FUNCTION_ROLE_ARN>

Take note of the `RoleArn` in the output, marked ``CROSS_ACCOUNT_ROLE`` in the next step.

Then finally, update the lambda function stack in account A

.. code-block::

    aws --profile ${AWS_PROFILE:-default} cloudformation deploy --capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
    --template-file ./template.json --stack-name lamba-fn--ccloud-secrets-rotation \
    --parameters-override ConfluentApiMasterSecretArn=<SECRET_ARN> \
    ConfluentSecretsArns="arn:aws:secretsmanager:eu-west-1:0123456789012:secret:/kafka/eu-west-1/lkc-*" \
    ConfluentApiMasterRoleArn=<CROSS_ACCOUNT_ROLE>

