#!/usr/bin/env python
"""
Lambda Function to rotate secret in secrets manager for the SASL credentials of confluent cloud
"""
from __future__ import annotations

import json
import logging
import os
import re
from copy import deepcopy

from boto3.session import Session
from compose_x_common.aws import get_assume_role_session, get_session
from compose_x_common.compose_x_common import keyisset
from confluent_cloud_sdk.client_factory import ConfluentClient
from confluent_cloud_sdk.confluent_iam_v2 import ApiKey
from confluent_cloud_sdk.errors import ConfluentApiException, GenericNotFound

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def replace_string_in_dict_values(
    input_dict: dict, src_value: str, new_value: str, copy: bool = False
):
    """
    :param input_dict:
    :param src_value:
    :param new_value:
    :return:
    """

    src_re = re.compile(re.escape(src_value))
    if copy:
        updated_dict = deepcopy(input_dict)
    else:
        updated_dict = input_dict
    for key, value in updated_dict.items():
        if isinstance(value, str) and src_re.findall(value):
            updated_dict[key] = src_re.sub(new_value, value)
        elif not isinstance(value, str):
            print(f"The value for {key} is not a string. Skipping")
    if copy:
        return updated_dict


def replace_kafka_credentials(
    previous_key: ApiKey, current_value: dict, confluent_client: ConfluentClient
):
    new_api_key = ApiKey(
        confluent_client,
        owner_id=previous_key.owner_id,
        resource_id=previous_key.resource_id,
    )
    new_api_key.create(
        owner_id=previous_key.owner_id, resource_id=previous_key.resource_id
    )
    new_value = replace_string_in_dict_values(
        current_value, current_value["SASL_USERNAME"], new_api_key.obj_id, True
    )
    replace_string_in_dict_values(
        new_value, current_value["SASL_PASSWORD"], new_api_key._secret
    )
    return new_value


def get_confluent_cloud_client(session: Session = None) -> ConfluentClient:
    iam_client_session = get_session(session)
    confluent_api_secret = os.environ.get("CONFLUENT_API_SECRET_ARN", None)
    logger.info(f"Confluent API MasterARN Secret: {confluent_api_secret}")
    if not confluent_api_secret:
        raise EnvironmentError("CONFLUENT_API_SECRET_ARN is not set.")
    confluent_credentials_role = os.environ.get("CONFLUENT_CREDENTIALS_ROLE", None)
    if confluent_credentials_role:
        secrets_session = get_assume_role_session(
            iam_client_session,
            confluent_credentials_role,
        )
        client = secrets_session.client("secretsmanager")
        logger.info(
            f"Client created using cross-account session with IAM Role {confluent_credentials_role}"
        )
    else:
        client = iam_client_session.client("secretsmanager")
    admin_secret = json.loads(
        client.get_secret_value(SecretId=confluent_api_secret)["SecretString"]
    )
    api_key = os.environ.get("CONFLUENT_API_KEY", "CCLOUD_API_KEY")
    secret_key = os.environ.get("CONFLUENT_SECRET_KEY", "CCLOUD_SECRET_KEY")
    return ConfluentClient(admin_secret[api_key], admin_secret[secret_key])


def validate_secret_stage(service_client, arn: str, token: str) -> bool:
    metadata = service_client.describe_secret(SecretId=arn)
    if not metadata["RotationEnabled"]:
        logger.error("Secret %s is not enabled for rotation" % arn)
        raise ValueError("Secret %s is not enabled for rotation" % arn)
    versions = metadata["VersionIdsToStages"]
    if token not in versions:
        logger.error(
            "Secret version %s has no stage for rotation of secret %s." % (token, arn)
        )
        raise ValueError(
            "Secret version %s has no stage for rotation of secret %s." % (token, arn)
        )
    if "AWSCURRENT" in versions[token]:
        logger.info(
            "Secret version %s already set as AWSCURRENT for secret %s." % (token, arn)
        )
        return True
    elif "AWSPENDING" not in versions[token]:
        logger.error(
            "Secret version %s not set as AWSPENDING for rotation of secret %s."
            % (token, arn)
        )
        raise ValueError(
            "Secret version %s not set as AWSPENDING for rotation of secret %s."
            % (token, arn)
        )
    return False


def lambda_handler(event: dict, context: dict):
    """
    Lambda handler to rotate secret in the context of secrets manager

    :param event:
    :param context:
    """

    arn = event["SecretId"]
    token = event["ClientRequestToken"]
    step = event["Step"]

    lambda_session = Session()
    confluent_api_client = get_confluent_cloud_client(lambda_session)
    service_client = lambda_session.client("secretsmanager")

    # Make sure the version is staged correctly
    if validate_secret_stage(service_client, arn, token):
        return

    if step == "createSecret":
        create_secret(arn, token, confluent_api_client, lambda_session)

    elif step == "setSecret":
        logger.debug("Nothing to set for confluent cloud")

    elif step == "testSecret":
        test_secret(arn, token, confluent_api_client, lambda_session)

    elif step == "finishSecret":
        finish_secret(arn, token, lambda_session)

    else:
        raise ValueError("Invalid step parameter")


def create_secret(
    arn: str, token: str, confluent_client: ConfluentClient, session: Session = None
) -> None:
    """
    Create the secret

    :param str arn: The secret ARN or other identifier
    :param str token: The ClientRequestToken associated with the secret version
    :param boto3.session.Session session:
    :param ConfluentClient confluent_client:
    :raises: ResourceNotFoundException: If the secret with the specified arn and stage does not exist
    """
    session = get_session(session)
    service_client = session.client("secretsmanager")

    current_value = service_client.get_secret_value(
        SecretId=arn, VersionStage="AWSCURRENT"
    )["SecretString"]
    if isinstance(current_value, str):
        current_value = json.loads(current_value)
        required_keys = [
            "SASL_USERNAME",
            "SASL_PASSWORD",
        ]
        if (
            "SASL_USERNAME" not in current_value.keys()
            or "SASL_PASSWORD" not in current_value.keys()
        ):
            raise KeyError(
                required_keys, "must be present in secret. Got", current_value.keys()
            )

    try:
        service_client.get_secret_value(
            SecretId=arn, VersionId=token, VersionStage="AWSPENDING"
        )
        logger.info("createSecret: Successfully retrieved secret for %s." % arn)
    except service_client.exceptions.ResourceNotFoundException:
        previous_key_details = ApiKey(
            confluent_client, obj_id=current_value["SASL_USERNAME"]
        )
        previous_key_details.set_from_read(key_id=current_value["SASL_USERNAME"])
        if not previous_key_details:
            raise LookupError(
                f"Failed to find the key {current_value['SASL_USERNAME']}"
            )
        try:
            new_value = replace_kafka_credentials(
                previous_key_details, current_value, confluent_client
            )
            service_client.put_secret_value(
                SecretId=arn,
                ClientRequestToken=token,
                SecretString=json.dumps(new_value),
                VersionStages=["AWSPENDING"],
            )
            logger.info(
                "createSecret: Successfully put secret for ARN %s and version %s."
                % (arn, token)
            )
        except Exception as error:
            logger.error(
                "createSecret: Failed to create a new secret for ARN %s and version %s."
                % (arn, token)
            )
            logger.error(error)


def test_secret(
    arn: str,
    token: str,
    confluent_client: ConfluentClient,
    lambda_session: Session = None,
):
    """
    Test the secret
    """

    lambda_session = get_session(lambda_session)
    service_client = lambda_session.client("secretsmanager")
    temp_secret = service_client.get_secret_value(
        SecretId=arn, VersionId=token, VersionStage="AWSPENDING"
    )["SecretString"]
    if isinstance(temp_secret, str):
        temp_secret = json.loads(temp_secret)
    api_key = ApiKey(confluent_client, obj_id=temp_secret["SASL_USERNAME"])
    try:
        api_key.read().json()
        api_key.set_from_read()
        logger.info(f"New key for {api_key.owner_id} - {api_key.resource_id} set.")
        logger.debug(api_key.obj_id)
        return
    except (GenericNotFound, ConfluentApiException):
        raise LookupError(
            f"Failed to confirm that key {temp_secret['SASL_USERNAME']} exists"
        )


def finish_secret(arn, token, lambda_session=None):
    """
    Finish the secret
    """

    lambda_session = get_session(lambda_session)
    service_client = lambda_session.client("secretsmanager")
    metadata = service_client.describe_secret(SecretId=arn)
    current_version = None
    for version in metadata["VersionIdsToStages"]:
        if "AWSCURRENT" in metadata["VersionIdsToStages"][version]:
            if version == token:
                # The correct version is already marked as current, return
                logger.info(
                    "finishSecret: Version %s already marked as AWSCURRENT for %s"
                    % (version, arn)
                )
                return
            current_version = version
            break

    service_client.update_secret_version_stage(
        SecretId=arn,
        VersionStage="AWSCURRENT",
        MoveToVersionId=token,
        RemoveFromVersionId=current_version,
    )
    logger.info(
        "finishSecret: Successfully set AWSCURRENT stage to version %s for secret %s."
        % (token, arn)
    )
