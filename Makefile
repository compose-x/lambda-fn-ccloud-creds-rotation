.PHONY: clean clean-test clean-pyc clean-build docs help
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

ifndef PYTHON_PACKAGE
PYTHON_PACKAGE	:= confluent_cloud_creds_rotation
endif

ifndef PYTHON_VERSION:
PYTHON_VERSION := python39
endif


help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -rf {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

lint: ## check style with flake8
	flake8 confluent_cloud_creds_rotation tests

test: ## run tests quickly with the default Python
	pytest -svx tests/

test-all: ## run tests on every Python version with tox
	tox

coverage: ## check code coverage quickly with the default Python
	coverage run --source confluent_cloud_creds_rotation -m pytest
	coverage report -m
	coverage html
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	rm -f docs/confluent_cloud_creds_rotation.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ confluent_cloud_creds_rotation
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

conform	: ## Conform to a standard of coding syntax
	isort --profile black confluent_cloud_creds_rotation.py
	black confluent_cloud_creds_rotation.py tests
	#find . -name "*.json" -type f  -exec sed -i '1s/^\xEF\xBB\xBF//' {} +


docker-clean:
			docker run --rm -it -v $(PWD):/app public.ecr.aws/amazonlinux/amazonlinux:2 rm -rf /app/build

python39	: dist
			docker run --network=host \
			--rm -it -v $(PWD):/app --entrypoint /bin/bash \
			public.ecr.aws/lambda/python:3.9 \
			-c "pip install pip pyclean -U;  pip install /app/dist/*.whl -t /app/build/python/lib/python3.9/site-packages/; pyclean /app/build/python/lib/python3.9/site-packages/" 1>/dev/null

python38	: dist
			docker run --network=host \
			--rm -it -v $(PWD):/app --entrypoint /bin/bash \
			public.ecr.aws/lambda/python:3.8 \
			-c "pip install pip pyclean -U;  pip install /app/dist/*.whl -t /app/build/python/lib/python3.8/site-packages/; pyclean /app/build/python/lib/python3.8/site-packages/" 1>/dev/null

python37	: dist
			docker run --network=host \
			--rm -it -v $(PWD):/app --entrypoint /bin/bash \
			public.ecr.aws/lambda/python:3.7 \
			-c "pip install pip pyclean -U; pip install /app/dist/*.whl  -t /app/build/python/lib/python3.7/site-packages/; pyclean /app/build/python/lib/python3.7/site-packages/" 1>/dev/null


dist:		conform ## builds source and wheel package
			poetry build 1>/dev/null

layers:		docker-clean
			$(MAKE) python37
			$(MAKE) python38
			$(MAKE) python39

zip:		layers
			rm -rf layer.zip
			cd build; zip -q -r9 ../layer.zip *; cd -

upload-zip-s3:
				if ! [ -f layer.zip ]; then $(MAKE) zip; fi
				cfn-flip function.yaml function.json
				aws cloudformation package --template-file function.json \
				--s3-bucket $(BUCKET) --s3-prefix $(PYTHON_PACKAGE) \
				--output-template-file template.json --use-json --force-upload
